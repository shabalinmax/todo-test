import React from 'react';
import './AddTodo.css'
const AddTodo = ({setUploadFilesToTodo, setNewTodoDescription, setNewTodoTitle, createNewTodo, setIsOpenAddTodoModal, setNewTodoDeadline, newTodoError, setNewTodoError }) => {

    /**
     * закрывает модальное окно
     *
     */
    const closeAddTodo = () => {
        setUploadFilesToTodo([])
        setNewTodoDescription('')
        setNewTodoTitle('')
        setNewTodoDeadline('')
        setIsOpenAddTodoModal(false)
        setNewTodoError(false)
    }
    return (
        <div className={'addTodoWrapper'} onClick={(e) => closeAddTodo()} >
            <div onClick={(e) => e.stopPropagation()} className={'addTodo'}>
                <input className={'defaultInput'} onChange={(e) => setNewTodoTitle(e.target.value)} type="text" placeholder={'Заголовок...'}/>
                <input className={'defaultInput'} onChange={(e) => setNewTodoDescription(e.target.value)}  type="text" placeholder={'Описание...'}/>
                <input type="file" onChange={(e) => {setUploadFilesToTodo([e.target.files[0]])}} multiple={false} />
                <div className="deadline">
                    <span>Выберите дедлайн: </span>
                    <input className={'defaultDatePicker'} placeholder={'asas'} type="date" onChange={(e) => setNewTodoDeadline(e.target.valueAsNumber)}/>
                </div>
                <button onClick={() => createNewTodo()} className={'defaultBtn'}>отправить задание</button>
                {newTodoError ? <p style={{color:'red', marginTop: '10px'}}>Все поля, кроме файлов являются обязвтельными!</p> : ''}
            </div>
        </div>
    );
};

export default AddTodo;
