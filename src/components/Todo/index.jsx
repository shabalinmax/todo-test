import React from 'react';
import { doc, updateDoc, deleteDoc } from "firebase/firestore";
import './Todo.css'
import dayjs from "dayjs";
import {db} from "../../firebase";
import axios from "axios";
const Todo = ({todo, deleteTodo,todoToggleComplete, getTodosFromFirebase}) => {
    let now = dayjs()
    todo.deadline = dayjs(todo.deadline) // converts the format to the desired one
    const [changeTodoTitle, setChangeTodoTitle] = React.useState('')
    const [changeTodoDescription, setChangeTodoDescription] = React.useState('')
    const [changeTodoDate, setChangeTodoDate] = React.useState(null)
    const [isChangeOpen, setIsChangeOpen] = React.useState(false)

    /**
     * изменяет туду и подкачивается изменные данные с базы
     * @returns {Promise<array>}
     *
     */
    const changeTodo = async () => {
        const todoRef = doc(db, 'todos', `${todo.todoDatabaseName}`)
        if (changeTodoTitle !== '') {
            await updateDoc(todoRef, {
                title: changeTodoTitle,
            })
            setChangeTodoTitle('')
        }
        if (changeTodoDescription !== '') {
            await updateDoc(todoRef, {
                description: changeTodoDescription,
            })
            setChangeTodoDescription('')
        }
        if (changeTodoDate !== null) {
            await updateDoc(todoRef, {
                deadline: changeTodoDate,
            })
            setChangeTodoDescription('')
        }
        setIsChangeOpen(false)
        await getTodosFromFirebase()
    }

    /**
     * позволяется скачать прикрепленные файлы к туду
     */
    const getTodoAttachedFiles = () => {
            axios({
                url:(`${todo.hasAttemptFile}`),
                method: 'GET',
                responseType: 'blob',
            }).then((response) => {
                const href = URL.createObjectURL(response.data);
                const link = document.createElement('a');
                link.href = href;
                link.setAttribute('download', ``);
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                URL.revokeObjectURL(href);
            })
        }
    return (
        <div className="todo" >
            <input className={'todoCheckbox'} onChange={() => todoToggleComplete(todo)} type={'checkbox'} defaultChecked={todo.isDone} ></input>
            <div className="todoTitle">
                <h2>{todo.title}</h2>
            </div>
            <div className="todoDescription">
                <p>{todo.description}</p>
            </div>
            <div className={'todoDeadline'}>
               <p style={now > todo.deadline ? {color: 'red'} : {} } >deadline: {todo.deadline.$D + '/' + (todo.deadline.$M + 1) + '/' + todo.deadline.$y }</p>
            </div>
            <div className="todoAttachedFiles">
                <button  className={'defaultBtn'} onClick={() => getTodoAttachedFiles()} disabled={!todo.hasAttemptFile.length}> download attached files</button>
            </div>

            <div className="todoDelete">
                <button className={'defaultBtn'} onClick={() => deleteTodo(todo)} >delete todo</button>
            </div>
            <div className="todoChange">
                <button onClick={() => setIsChangeOpen(!isChangeOpen)} className={'defaultBtn'} >change todo</button>
                <div className={'todoChangeInputs'} style={isChangeOpen ? {display: "block"} : {display: "none"}} >
                    <input onChange={(e) => setChangeTodoTitle(e.target.value)} placeholder={todo.title} type="text"/>
                    <input onChange={(e) => setChangeTodoDescription(e.target.value)} placeholder={todo.description} type="text"/>
                    <input onChange={(e) => setChangeTodoDate(e.target.valueAsNumber)} type="date" />
                    <button className={'defaultBtn'} onClick={() => changeTodo()} >save</button>
                </div>
            </div>
        </div>
    );
};

export default Todo;
