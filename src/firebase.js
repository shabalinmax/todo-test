import { initializeApp } from "firebase/app";
import {getStorage} from 'firebase/storage'
import {getFirestore} from 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyAEDPl9mXyspMyzTNwJbNginRW_Ul_pk10",
    authDomain: "todo-testovoe.firebaseapp.com",
    projectId: "todo-testovoe",
    storageBucket: "todo-testovoe.appspot.com",
    messagingSenderId: "621200849752",
    appId: "1:621200849752:web:b9ee3f08a46119efc9832d"
};


const app = initializeApp(firebaseConfig);

export const db = getFirestore(app)
export const storage = getStorage(app)