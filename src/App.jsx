import {db, storage} from "./firebase";
import {ref,  getDownloadURL, uploadBytesResumable  } from 'firebase/storage'
import React from "react";
import './App.css'
import Todo from "./components/Todo";
import AddTodo from "./components/AddTodo";
import {doc, getDocs, setDoc, collection, updateDoc, deleteDoc} from "firebase/firestore";
function App() {

    const [uploadFilesToTodo, setUploadFilesToTodo] = React.useState([])
    const [uploadedFilesLink, setUploadedFilesLink] = React.useState('')
    const [newTodoTitle, setNewTodoTitle] = React.useState('')
    const [newTodoDescription, setNewTodoDescription] = React.useState('')
    const [newTodoDeadline, setNewTodoDeadline] = React.useState('')
    const [newTodoError, setNewTodoError] = React.useState(false)
    const [todos, setTodos] = React.useState([])
    const [isOpenAddTodoModal, setIsOpenAddTodoModal] = React.useState(false)

    /**
     * удаляет туду из базы
     * @param {object} todo - туду, который хотим удалить
     * @returns {Promise<void>}
     */
    const deleteTodo = async (todo) => {
        setTodos(todos.filter(el => el.id !== todo.id))
        await deleteDoc(doc(db, 'todos', `${todo.todoDatabaseName}`))
    }

    /**
     * позволяет отметить выполнена ли туду
     * @param {object} todo - туду, который хотим отметить
     * @returns {Promise<void>}
     */
    const todoToggleComplete = async (todo) => {
        todos.find((el) => el.id === todo.id).isDone = !todos.find((el) => el.id === todo.id).isDone
        const todoRef = doc(db, 'todos', `${todo.todoDatabaseName}`)
            await updateDoc(todoRef, {
                isDone: todo.isDone
            })
        }

    /**
     * позволяет отправить новую туду в базу
     * @param {object} newTodo - новая туду
     * @returns {Promise<void>}
     */
    const sendToDb = async (newTodo) => {
        setDoc(doc(db, `todos`, `${newTodoTitle}`), newTodo )
    }

    /**
     * загружает файл в базу
     * @param file -
     * @returns {Promise<string>} - возравает строку с ссылкой на файл
     */
    const uploadFile = (file) => {
        const storageRef = ref(storage, `${file.name}`)
        const uploadTask = uploadBytesResumable(storageRef, file)
        return new Promise((resolve, reject) => {
            uploadTask.on(
                "state_changed",
                (snapshot) => {
                },
                (error) => reject(error),
                () => (
                    getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                        resolve(downloadURL)
                    })
                )
            )
        })
    }

    /**
     * функция позволяет получить все туду из базы
     * @returns {Promise<array>} - возвращает массив туду
     */
    async function getTodosFromFirebase(){
        const request = await getDocs(collection(db, 'todos'))
        const todos = []
        request.forEach((doc) => todos.push(doc.data()))
        setTodos([...todos])
    }

    /**
     * позволяется добавить новую туду в базу
     * @returns {Promise<void>}
     */
    const createNewTodo = async () => {
        if (newTodoTitle !== '' && newTodoDescription !== '' && newTodoDeadline !== '') {
            uploadFilesToTodo.length > 0 ?
                uploadFile(uploadFilesToTodo[0])
                .then((res) =>
                sendToDb({
                    title: newTodoTitle,
                    id: Date(),
                    description: newTodoDescription,
                    deadline: newTodoDeadline,
                    isDone: false,
                    hasAttemptFile: res,
                    todoDatabaseName: newTodoTitle,
                }))
                    .then(() => {
                        getTodosFromFirebase()
                    })
                :
                sendToDb({
                    title: newTodoTitle,
                    id: Date(),
                    description: newTodoDescription,
                    deadline: newTodoDeadline,
                    isDone: false,
                    hasAttemptFile: '',
                    todoDatabaseName: newTodoTitle,
                })
                    .then(() => {
                        getTodosFromFirebase()
                     })

            setIsOpenAddTodoModal(false)
            setNewTodoDescription('')
            setNewTodoTitle('')
            setNewTodoDeadline('')
        }
        else {
            setNewTodoError(true)
        }
    }

    React.useEffect(() => {
        getTodosFromFirebase()
    },[])

    return (
    <div className="App">
        {isOpenAddTodoModal ?
            <AddTodo
                setUploadFilesToTodo={setUploadFilesToTodo}
                setNewTodoTitle={setNewTodoTitle}
                setNewTodoDescription={setNewTodoDescription}
                createNewTodo={createNewTodo}
                setIsOpenAddTodoModal={setIsOpenAddTodoModal}
                setNewTodoDeadline={setNewTodoDeadline}
                newTodoError={newTodoError}
                setNewTodoError={setNewTodoError}/>
            : null}

            <button className="defaultBtn" onClick={() => setIsOpenAddTodoModal(true)} >Добавить задание</button>

        <div className="todos">
            {todos.map((todo) =>
                <Todo
                    key={todo.id}
                    todo={todo}
                    deleteTodo={deleteTodo}
                    todoToggleComplete={todoToggleComplete}
                    getTodosFromFirebase={getTodosFromFirebase}
                />
            )}
        </div>
    </div>
  );
}

export default App;
