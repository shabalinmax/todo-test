FROM node:14-alpine
WORKDIR /app
CMD serve -n -s build
RUN npm install --global serve
COPY package*.json ./
RUN npm ci
COPY public public
COPY src src
RUN npm run build


